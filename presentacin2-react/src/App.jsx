import { useState } from 'react'
import './App.css'
import {C1} from './components/C1';
import {C2} from './components/C2'

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="container-items">
        <C1 className="AAA"></C1>
        <C2></C2>
    </div>
  )
}

export default App
